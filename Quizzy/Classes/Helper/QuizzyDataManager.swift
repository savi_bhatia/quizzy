//
//  QuestionViewModel.swift
//


import Foundation

class QuizzyDataManager {
    
    var quizResponse:QuizResponse?
    var answers = [String]()
    var quizOptionsList = [String?]()
    var trivia_categories : Trivia_categories?
    
    private let category_url = URL(string:"https://opentdb.com/api_category.php")!
    
    func apiToGetQuestionData(completion : @escaping () -> ()) {
        let categoryType = UserDefaults.standard.value(forKey: "CategoryId") ?? "9"
        let questionType = UserDefaults.standard.value(forKey: "questionType") ?? "multiple"
        let difficultyLevel = UserDefaults.standard.value(forKey: "difficultyLevel") ?? "easy"

        let sourcesURL = ((URL(string: "https://opentdb.com/api.php?amount=10&category=\(categoryType )&difficulty=easy&type=\(questionType)")) ?? ((URL(string: "https://opentdb.com/api.php?amount=10&category=9&difficulty=medium&type=multiple"))))!
                            
        URLSession.shared.dataTask(with: sourcesURL) { [weak self] (data, urlResponse, error) in
            if let data = data {
                let decoder = JSONDecoder()
                
                do {
                    self?.quizResponse = try decoder.decode(QuizResponse.self, from: data)
                    UserDefaults.standard.set(data, forKey: "QuizResponse")
                    print("Response code: \(self?.quizResponse?.responseCode)")
                    print("Number of results: \(self?.quizResponse?.results.count)")
                    for question in self?.quizResponse?.results ?? [] {
                        print("Question: \(question.question)")
                        print("Correct Answer: \(question.correctAnswer)")
                        print("Incorrect Answers: \(question.incorrectAnswers)")
                        
                    }
                    completion()
                } catch {
                    print("Error parsing JSON: \(error)")
                }
            }
        }.resume()
        
       }

    func fetchCategories(completionHandler: @escaping ([Trivia_categories]) -> Void) {
           
        
      let task = URLSession.shared.dataTask(with: category_url, completionHandler: { (data, response, error) in
        if let error = error {
          print("Error with fetching quiz data: \(error)")
          return
        }
        
        guard let httpResponse = response as? HTTPURLResponse,
              (200...299).contains(httpResponse.statusCode) else {
          print("Error with the response, unexpected status code: \(response)")
          return
        }

        if let data = data,
          let openTriviaCategoryResponse = try? JSONDecoder().decode(OpenTriviaCategoryResponse.self, from: data) {
            completionHandler(openTriviaCategoryResponse.trivia_categories ?? [])
        }
      })
      task.resume()
    }
    func loadQuizResponse() -> QuizResponse? {
            guard let data = UserDefaults.standard.data(forKey: "QuizResponse") else {
                return nil
            }
            
            do {
                let quizResponse = try JSONDecoder().decode(QuizResponse.self, from: data)
                return quizResponse
            } catch {
                print("Error decoding quiz response: \(error)")
                return nil
            }
        }
}

