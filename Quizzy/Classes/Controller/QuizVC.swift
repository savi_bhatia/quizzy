//
//  QuizVC.swift
//  Quizzy
//
//  Created by Savi Bhatia on 26/04/23.
//

import UIKit

class QuizVC: UIViewController {
    
    enum QuizStage {
        case Start
        case Submit
    }
    //MARK:- VARIABLE DECLARATION
    var quizOptionsList = [String?]()
    var viewModel = QuizzyDataManager()
    var answers = [String]()
    var correctAnswer: String!
    var quizName = ""
    var count = 0
    var quizType = ""
    var quizStage : QuizStage = .Start
    var answerSelected = false
    var isCorrectAnswer = false
    var  points = 0
    var question: String!
    var questionCount = 10
    var timerCounter: Timer?
    var totalTimeGiven = 300
    var totalTimeTaken = 0
    var timeCount = 300
    var attemptedQuestion = 0
    var titleArr = ["Quiz Name","Time","Time Taken","Total Question","Attempted Question"]
    var startQuizArr = ["Quiz Name","Category Name","No. of Questions","Time"]
    var startPracticeQuizArr = ["Quiz Name","Category Name","No. of Questions"]
    var titlePracticeArr = ["Quiz Name","Total Question","Attempted Question"]
    var selectedAnswer = ""
    var selectedAnswersArr = [String]()

    
    //MARK:- OUTLET DECLARATION
    @IBOutlet weak var questionView: UIView!
    @IBOutlet weak var resultTblView: UITableView!
    @IBOutlet weak var resultAlphaView: UIView!
    @IBOutlet weak var resultView: UIView!
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var outOfLbl: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var resultSubmitBtn: UIButton!
    @IBOutlet weak var quizQuestion: UILabel!
    @IBOutlet weak var nextQuestionBtn: UIButton!
    @IBOutlet weak var quizCategory: UILabel!
    @IBOutlet weak var optionTableView: UITableView!

    //MARK:- VIEW DECLARATION
    override func viewDidLoad() {
        super.viewDidLoad()
        submitBtn.layer.cornerRadius = 20
        questionView.layer.cornerRadius = 40
        resultSubmitBtn.layer.cornerRadius = 20
        loadData()
       
    }
    
    func loadData()
    {
        if self.quizType == "viewSolution"
        {
            self.timerView.isHidden = true
            self.resultAlphaView.isHidden = true
            self.resultView.isHidden = true
            self.submitBtn.isHidden = true
            self.quizCategory.text = "Solution"
            let quizRes = viewModel.loadQuizResponse()
            self.dataStorage()
        }
        else
        {
            self.quizName = UserDefaults.standard.value(forKey: "CategoryId") as? String ?? "9"
            self.quizCategory.text = self.quizName
            viewModel.apiToGetQuestionData { [weak self] in
                self?.dataStorage()
            }
        }

    }
    
    //MARK:- FUNCTIONS DECLARATION
    func updateUI() {
        DispatchQueue.main.async {
            self.quizCategory.text = self.quizName
            self.changeQuestion(question: self.question)
            self.quizOptionsList.shuffle()
            self.question = self.viewModel.quizResponse?.results[self.count].question
            if self.quizStage != .Start {
                self.resultSubmitBtn.setTitle("Submit", for: .normal)
            }
            self.dismiss(animated: true, completion: nil)
            self.outOfLbl.text = "\(self.count + 1)/\(self.questionCount)"
            if self.quizType == "Practice" {
                self.timerView.isHidden = true
            }
            else if self.quizType == "Live"{
                self.timerCounter = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) {
                    (timer) in
                    
                    if self.timeCount > 0 {
                        self.timeCount -= 1
                        self.timerLabel.text = self.timeString(time: TimeInterval(self.timeCount))
                    } else {
                        self.totalTimeTaken = self.totalTimeGiven - self.timeCount
                        timer.invalidate()
                        self.nextButtonAction()
                        self.timerLabel.text = "Time's up!"
                    }
                }
            }
            
            self.nextQuestionBtn.isEnabled = true
            self.resultTblView.reloadData()
            self.optionTableView.reloadData()

        }
    }
    
    
    func dataStorage() {
        if self.quizType == "viewSolution"
        {
            
            let responseModel = self.viewModel.loadQuizResponse()?.results
            self.question = responseModel?[self.count].question
            self.quizOptionsList.append(responseModel?[self.count].correctAnswer)
            self.quizOptionsList = [responseModel?[self.count].correctAnswer, responseModel?[self.count].incorrectAnswers[0], responseModel?[self.count].incorrectAnswers[1], responseModel?[self.count].incorrectAnswers[2]]
            self.correctAnswer = responseModel?[self.count].correctAnswer

        }
        else {
            if let responseModel = self.viewModel.quizResponse?.results {
                if responseModel.count < 10 {
                    count = responseModel.count
                }
                self.quizName = responseModel[self.count].category
                self.question = responseModel[self.count].question
                self.quizOptionsList = [responseModel[self.count].correctAnswer, responseModel[self.count].incorrectAnswers[0], responseModel[self.count].incorrectAnswers[1], responseModel[self.count].incorrectAnswers[2]]
                self.correctAnswer = responseModel[self.count].correctAnswer
                
            }}
        
        self.quizOptionsList.shuffle()
        updateUI()
    }
    
    func nextButtonAction() {
        nextQuestionBtn.isEnabled = false
        selectedAnswersArr.append(selectedAnswer)
        if selectedAnswer == correctAnswer
        {
            points += 1
            print(points)
        }
        selectedAnswer = ""

        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(0), execute: {
            self.count = self.count + 1
            if self.count < 10 {
                self.dataStorage()
                self.optionTableView.reloadData()

            }
            else {
                self.quizStage = .Submit
                self.resultSubmitBtn.setTitle("Submit", for: .normal)
                self.totalTimeTaken = self.totalTimeGiven - self.timeCount
                self.timerCounter?.invalidate()
                self.resultAlphaView.isHidden = false
                self.resultView.isHidden = false
                self.resultTblView.reloadData()
            }
        })
    }
    func timeString(time: TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    func changeQuestion(question: String) {
        quizQuestion.text = "Q\(count + 1). \(question)"
    }
    
    func changeQuizOptions(quizOptionButton: UIButton, optionText: String) {
        quizOptionButton.setTitle(optionText, for: .normal)
    }
    
    //MARK:- IBACTIONS
    @IBAction func nextBtnClicked(_ sender: Any) {
       nextButtonAction()
    }
   
    @IBAction func goBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func SubmitQuiz(_ sender:Any) {
     
            if count >= 10 {
                self.resultTblView.reloadData()
                self.resultAlphaView.isHidden = false
                self.resultView.isHidden = false
            }
            else {
                nextButtonAction()
            }
        
    }
   
    @IBAction func ResultSubmitBtn(_ sender: Any) {
        if self.quizStage == .Start {
            self.optionTableView.reloadData()
            resultAlphaView.isHidden = true
            resultView.isHidden = true
        }
        else {
            guard let ResultQuizVC =  storyboard?.instantiateViewController(withIdentifier: "ResultViewController") as? ResultViewController else {return}
            ResultQuizVC.timeSpent = "\(self.timeString(time: TimeInterval(totalTimeTaken)))"
            ResultQuizVC.totalScore = "\(points)"
            ResultQuizVC.quizType = quizType
            ResultQuizVC.selectedAnswersArr = selectedAnswersArr
            self.navigationController?.pushViewController(ResultQuizVC, animated: true)
            
        }}
}

//EXTENSION TABLEVIEW
extension QuizVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == resultTblView {
            let viewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ResultCell
            if quizType == "Practice" {
                if self.quizStage == .Submit {
                    viewCell.QuizLbl?.text = self.titlePracticeArr[indexPath.row]
                    if indexPath.row == 0 {
                        viewCell.LblAnswer.text = quizName
                    }
                    if indexPath.row == 1 {
                        viewCell.LblAnswer.text = "\(self.questionCount)"
                    }
                    if indexPath.row == 2 {
                        viewCell.LblAnswer.text = "\(self.attemptedQuestion)"
                    }}
                else if quizType == "Live" {
                    viewCell.QuizLbl?.text = self.startPracticeQuizArr[indexPath.row]
                    
                    if indexPath.row == 0 {
                        viewCell.LblAnswer.text = quizName
                    }
                    if indexPath.row == 1 {
                        viewCell.LblAnswer.text = "English"
                    }
                    if indexPath.row == 2 {
                        viewCell.LblAnswer.text = "10"
                    }
                }
                
            }
            else {
                if self.quizStage == .Submit {
                    viewCell.QuizLbl?.text = self.titleArr[indexPath.row]
                    if indexPath.row == 0 {
                        viewCell.LblAnswer.text = quizName
                    }
                    if indexPath.row == 1 {
                        viewCell.LblAnswer.text = "\(self.timeString(time: TimeInterval(totalTimeGiven)))"
                    }
                    if indexPath.row == 2 {
                        viewCell.LblAnswer.text = "\(self.timeString(time: TimeInterval(totalTimeTaken)))"
                    }
                    if indexPath.row == 3 {
                        viewCell.LblAnswer.text = "\(self.questionCount)"
                    }
                    if indexPath.row == 4 {
                        viewCell.LblAnswer.text = "\(selectedAnswersArr.filter { !$0.isEmpty }.count)"
                    }}
                else {
                    viewCell.QuizLbl?.text = self.startQuizArr[indexPath.row]
                    
                    if indexPath.row == 0 {
                        viewCell.LblAnswer.text = quizName
                    }
                    if indexPath.row == 1 {
                        viewCell.LblAnswer.text = "English"
                    }
                    if indexPath.row == 2 {
                        viewCell.LblAnswer.text = "10"
                    }
                    if indexPath.row == 3 {
                        viewCell.LblAnswer.text = "\(self.timeString(time: TimeInterval(totalTimeGiven)))"
                    }
                }}
            return viewCell
        }
        else {
            let viewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! QuizOptionCell
            viewCell.optionView.layer.cornerRadius = 18
            viewCell.optionLbl?.text = quizOptionsList[indexPath.row]
            let backgroundView = UIView()
            backgroundView.backgroundColor = UIColor.clear
            viewCell.selectedBackgroundView = backgroundView
            if self.quizType == "viewSolution" {
                selectedAnswer = selectedAnswersArr[count]
                print("\(selectedAnswer) == \(correctAnswer)")
                
                if selectedAnswer == quizOptionsList[indexPath.row] {
                    viewCell.optionView.backgroundColor = UIColor.red
                }
                else {
                    viewCell.optionView.backgroundColor = UIColor(red: 0.325, green: 0.176, blue: 0.455, alpha: 1)
                }
                if quizOptionsList[indexPath.row] == correctAnswer {
                    viewCell.optionView.backgroundColor = UIColor(red: 0.204, green: 0.78, blue: 0.349, alpha: 1 )
                }
              
                  
              
            }
            else
            {
                if selectedAnswer == quizOptionsList[indexPath.row] {
                    viewCell.optionView.backgroundColor = UIColor(red: 0.204, green: 0.78, blue: 0.349, alpha: 1)
                }
                else {
                    viewCell.optionView.backgroundColor = UIColor(red: 0.325, green: 0.176, blue: 0.455, alpha: 1)
                }
            }
            return viewCell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == optionTableView {
            let cell:QuizOptionCell = tableView.cellForRow(at: indexPath) as! QuizOptionCell
            selectedAnswer = quizOptionsList[indexPath.row] ?? ""
            optionTableView.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        if tableView == resultTblView {
            if quizType == "Practice" {
                if self.quizStage == .Submit {
                    numberOfRows = self.titlePracticeArr.count
                }
                else {
                    numberOfRows = self.startPracticeQuizArr.count
                }
            }
            else {
                if self.quizStage == .Submit {
                    numberOfRows = self.titleArr.count
                }
                else {
                    numberOfRows = self.startQuizArr.count
                }
            }}
        else {
            numberOfRows = quizOptionsList.count
        }
        return numberOfRows
    }
}
