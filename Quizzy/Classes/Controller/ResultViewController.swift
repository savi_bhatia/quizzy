//
//  ResultViewController.swift
//  Quizzy
//
//  Created by Savi Bhatia on 04/05/23.
//

import UIKit

class ResultViewController: UIViewController {

    //MARK:- Variable Declarations
    var timeSpent : String = ""
    var totalScore : String = ""
    var quizType = ""
    var selectedAnswersArr = [String]()
    //MARK:- Outlet Declarations
    @IBOutlet weak var timeSpentLbl: UILabel!
    @IBOutlet weak var refreshBtn: UIButton!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var scoreLbl: UILabel!
    @IBOutlet weak var scoredView: UIView!
    @IBOutlet weak var timeSpentView: UIView!
    @IBOutlet weak var rankView: UIView!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewSolutionBtn: UIButton!
    @IBOutlet weak var reattemptBtn: UIButton!
    // MARK:- VIEWDIDLOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        // Do any additional setup after loading the view.
    }
    //
    func updateUI() {
        if quizType == "Practice" {
            timeSpentView.isHidden = true
            //StackViewHeight.constant = 240
        }
        nameView.layer.cornerRadius = 12
        scoredView.layer.cornerRadius = 12
        timeSpentView.layer.cornerRadius = 12
        viewSolutionBtn.layer.cornerRadius = 18
        reattemptBtn.layer.cornerRadius = 18
        refreshBtn.layer.cornerRadius = 18
        rankView.layer.cornerRadius = 12
        scoreLbl.text = totalScore
        timeSpentLbl.text = timeSpent

    }
    
    //MARK:- IBACTIONS
    @IBAction func goBckBtn(_ sender: Any) {
        guard let backToHomePage =  storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {return}
        self.navigationController?.pushViewController(backToHomePage, animated: true)
      
        
    }
    
    @IBAction func viewSolutionsBtn(_ sender: Any) {
        guard let solutionsVc =  storyboard?.instantiateViewController(withIdentifier: "QuizVC") as? QuizVC else {return}
        solutionsVc.quizType = "viewSolution"
        solutionsVc.selectedAnswersArr = selectedAnswersArr
        self.navigationController?.pushViewController(solutionsVc, animated: true)
    }
    

    @IBAction func reattemptBtn(_ sender: Any) {
        guard let startQuizVc =  storyboard?.instantiateViewController(withIdentifier: "QuizVC") as? QuizVC else {return}
        startQuizVc.quizType = "Live"
        self.navigationController?.pushViewController(startQuizVc, animated: true)
    }
}
