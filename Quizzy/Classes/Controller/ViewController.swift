//
//  ViewController.swift
//  Quizzy
//
//  Created by Savi Bhatia on 24/04/23.
//

import UIKit

class ViewController: UIViewController {

    //MARK :- OUTLET DECLARATION
    @IBOutlet weak var tblView: UITableView!
    
    //MARK :- VIEW LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}
//MARK :- TABLEVIEW
extension ViewController: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomePageCell", for: indexPath) as! HomePageTableCell
        cell.OuterView.layer.cornerRadius = 10
        if indexPath.row == 1
        {
            cell.TextTitle.text = "Normal Play"
            cell.cellBackgroundImg.image = UIImage(named: "blue_radient")
        }
        if indexPath.row == 2
        {
            cell.TextTitle.text = "Settings"
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            guard let startQuizVc =  storyboard?.instantiateViewController(withIdentifier: "QuizVC") as? QuizVC else {return}
            startQuizVc.quizType = "Live"
            self.navigationController?.pushViewController(startQuizVc, animated: true)
        }
        if indexPath.row == 1
        {
            guard let startQuizVc =  storyboard?.instantiateViewController(withIdentifier: "QuizVC") as? QuizVC else {return}
            startQuizVc.quizType = "Practice"
            self.navigationController?.pushViewController(startQuizVc, animated: true)
        }
        if indexPath.row == 2
        {
            guard let settingVc =  storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as? SettingViewController else {return}
            self.navigationController?.pushViewController(settingVc, animated: true)
        }
    }
}
