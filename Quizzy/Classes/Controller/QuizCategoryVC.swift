
//
//  QuizCategoryVC.swift
//  Quizzy
//
//  Created by Savi Bhatia on 09/05/23.
//

import UIKit

class QuizCategoryVC: UIViewController {
    
    // MARK: - Variable declarations
    var viewModel = QuizzyDataManager()
    var category_array: [Trivia_categories]?
    var selectedCateory = ""
    
    // MARK: - OUTLET DECLARATOn
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 6
        layout.minimumInteritemSpacing = 5
        categoryCollectionView.setCollectionViewLayout(layout, animated: true)
        viewModel.fetchCategories(){
            [weak self] (category) in
            
            self?.category_array = category
            print("_response is :", self!.category_array!);
            if self!.category_array != nil{
                self!.updateCategoryView()
            }
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func goBck(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func updateCategoryView() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
            self.categoryCollectionView.reloadData()
        }
    }
}
extension QuizCategoryVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.category_array?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "proCell", for: indexPath) as! QuizCategoryCollectionCell
        if self.category_array != nil {
            cell.categoryLabel.text = self.category_array![indexPath.row].name
            cell.categoryImage.image = UIImage(named: "Quiz-PNG-Image")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.category_array != nil {
            let selectedCategoryName = self.category_array![indexPath.row].name
            let selectedCategoryId = self.category_array![indexPath.row].id

            self.showToast(message: "Category updated", completion: nil)
            UserDefaults.standard.setValue(selectedCategoryId, forKey: "CategoryId")
            self.navigationController?.modalTransitionStyle = .crossDissolve
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                
                self.navigationController?.popViewController(animated: true)
                
            }
        }}
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
                    let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing

        return CGSize(width:widthPerItem, height:widthPerItem)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1.0, left: 1.0, bottom: 1.0, right: 1.0)
    }
    

}
extension UIViewController{
    func showToast(message: String, completion: (()->Void)?) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            alert.dismiss(animated: true, completion: {
                if let completion = completion {
                    completion()
                }
            })
        }
    }
    
}

