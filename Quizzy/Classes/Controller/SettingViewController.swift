//
//  SettingViewController.swift
//  Quizzy
//
//  Created by Savi Bhatia on 09/05/23.
//

import UIKit

class SettingViewController: UIViewController {

    @IBOutlet weak var questionTypePopUp: UIButton!
    @IBOutlet weak var difficultyPopUpBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpPopButton()
        // Do any additional setup after loading the view.
    }
    // MARK: - IBACTIONS
    @IBAction func SelectCategoryButton(_ sender: Any) {
        guard let backToHomePage =  storyboard?.instantiateViewController(withIdentifier: "QuizCategoryVC") as? QuizCategoryVC else {return}
        self.navigationController?.pushViewController(backToHomePage, animated: true)
    }
    @IBAction func goBckBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   func setUpPopButton()
    {
        let difficultyLevelClosure = {(action: UIAction) in
            print(action.title)
            if action.title == "Easy"
            {
                UserDefaults.standard.set("easy", forKey: "difficultyLevel")
            }
            if action.title == "Medium"
            {
                UserDefaults.standard.set("medium", forKey: "difficultyLevel")
            }
            if action.title == "Difficult"
            {
                UserDefaults.standard.set("difficult", forKey: "difficultyLevel")
            }
            
        }
        let questionTypeClosure = {(action: UIAction) in
            print(action.title)
            if action.title == "MCQ"
            {
                UserDefaults.standard.set("multiple", forKey: "questionType")
            }
            if action.title == "True/False"
            {
                UserDefaults.standard.set("boolean", forKey: "questionType")
            }
         
        }
        difficultyPopUpBtn.menu = UIMenu(children: [
            UIAction(title: "Easy",state: .on,handler: difficultyLevelClosure),
            UIAction(title: "Medium",handler: difficultyLevelClosure), UIAction(title: "Difficult",handler: difficultyLevelClosure)
        ])
        difficultyPopUpBtn.showsMenuAsPrimaryAction = true
        difficultyPopUpBtn.changesSelectionAsPrimaryAction = true
        questionTypePopUp.menu = UIMenu(children: [
            UIAction(title: "MCQ",state: .on,handler: questionTypeClosure),
            UIAction(title: "True/False",handler: questionTypeClosure)
        ])
        questionTypePopUp.showsMenuAsPrimaryAction = true
        questionTypePopUp.changesSelectionAsPrimaryAction = true
        }
    }


