//
//  DataModel.swift
//  Quizzy
//
//  Created by Savi Bhatia on 27/04/23.
//
import Foundation
struct QuizQuestion: Codable {
    let category: String
    let type: String
    let difficulty: String
    let question: String
    let correctAnswer: String
    let incorrectAnswers: [String]
    
    private enum CodingKeys: String, CodingKey {
        case category, type, difficulty, question
        case correctAnswer = "correct_answer"
        case incorrectAnswers = "incorrect_answers"
    }
}

struct QuizResponse: Codable {
    let responseCode: Int
    let results: [QuizQuestion]
    
    private enum CodingKeys: String, CodingKey {
        case responseCode = "response_code"
        case results
    }
}
