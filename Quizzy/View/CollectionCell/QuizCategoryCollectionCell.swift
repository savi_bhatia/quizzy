//
//  QuizCategoryCollectionCell.swift
//  Quizzy
//
//  Created by Savi Bhatia on 09/05/23.
//

import UIKit

class QuizCategoryCollectionCell: UICollectionViewCell {
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
}
