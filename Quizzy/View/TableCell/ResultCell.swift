//
//  ResultCell.swift
//  Quizzy
//
//  Created by Savi Bhatia on 05/05/23.
//

import UIKit

class ResultCell: UITableViewCell {

    @IBOutlet weak var LblAnswer: UILabel!
    @IBOutlet weak var QuizLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
