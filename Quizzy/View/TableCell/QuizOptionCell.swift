//
//  QuizCVCell.swift
//  Quizzy
//
//  Created by Savi Bhatia on 27/04/23.
//

import UIKit

class QuizOptionCell: UITableViewCell {
    @IBOutlet weak var optionLbl: UILabel!
    
    @IBOutlet weak var optionView: UIView!
}
