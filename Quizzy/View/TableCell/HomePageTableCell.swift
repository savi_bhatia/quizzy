//
//  HomePageTableCell.swift
//  Quizzy
//
//  Created by Savi Bhatia on 26/04/23.
//

import UIKit

class HomePageTableCell: UITableViewCell {

    @IBOutlet weak var TextTitle: UILabel!
    @IBOutlet weak var cellBackgroundImg: UIImageView!
    @IBOutlet weak var OuterView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
